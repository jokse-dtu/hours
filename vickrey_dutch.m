function OUT = vickrey_dutch(all_assignments, c, c0, t_limit, rule, verbose)
%VICKREY-DUTCH auction for procurement [@Mishra_Veeramani_2007, definition 4]
%   Assuming truthful bidding.

% Initialize the function
n = length(c);
dimensions = size(c0);
m = numel(c0);
if ~exist('verbose','var'); verbose = false; end
% Reformat each assignment to matrix form
all_assignments_matrix{1} = assign2matrix(all_assignments{1}, n+1, m); % Main economy
all_assignments_matrix(2:(n+1)) = repmat({assign2matrix(all_assignments{2}, n, m)}, 1, n); % Marginal economies
% Output metadata
OUT.class = 'vickrey_dutch';
OUT.c = c;
OUT.c0 = c0;
OUT.error_time = true;

% Step 0
% - Initialize the price vector to the zero price vector.
t = 1;
p_0{1} = zeros(dimensions);
OUT.prices{t} = repmat(p_0, n, 1);



while t < t_limit
    if verbose; fprintf('Round %d ... \n', t); end
    
    % Step 1
    % - Collect supply sets of suppliers at the current price vector p.
    for i=1:n
        % [Assuming truthful bidding]
        % Return the supply sets of supplier i
        [supply_sets, ~] = supply_correspondence(c{i}, OUT.prices{t}{i});
        OUT.supply_sets{t}{i} = supply_sets;
    end


    
    % Step 2
    % - If undersupply does not hold in economy E(M) for every M in B at the current price vector p, go to Step 3. 
    % - Else, select an economy E(M) in which undersupply holds at the current price vector p and do the price adjustment (Step 2.1).

    % Loop through each economy (1 main and n marginal)
    demand_sets         = cell(n+1, 1);
    procurement_cost    = cell(n+1, 1);
    ind_assignments_CE  = cell(n+1, 1);
    ind_assignments_qCE = cell(n+1, 1);
    for e = 1:(n+1)

        % Prepare/reformat price matrix (remove row of supplier e in marginal economies)
        % Price matrix for all suppliers including "in-house" (note p_0(S) = c_0(S) for all S).
        prices_matrix_e = [c0(:)'; reshape2mat(OUT.prices{t})];
        if e ~= 1; prices_matrix_e(e,:) = []; end
        
        % Return the demand set
        [demand_sets{e}, procurement_cost{e}] = demand_correspondence(all_assignments_matrix{e}, prices_matrix_e);

        
        % Prepare/reformat supply sets (remove cell of supplier e)
        supply_sets_e = OUT.supply_sets{t}(setdiff(1:n,e-1));
        supply_sets_e_compatible = cellfun(@(x) [x m], supply_sets_e, 'UniformOutput', false); % For the compatible demand set (adds index of the exhaustive bundle)
        
        % Assignment-indicies for the assignments that constitute a (restricted/quasi) competitive equilibrium.
        ind_assignments_CE{e}  = equilibrium(all_assignments{e}, demand_sets{e}, supply_sets_e);
        ind_assignments_qCE{e} = equilibrium(all_assignments{e}, demand_sets{e}, supply_sets_e_compatible); % Returns compatible demand set

    end
    OUT.demand_sets{t} = demand_sets;
    OUT.procurement_cost{t} = procurement_cost;
    OUT.ind_assignments_CE{t} = ind_assignments_CE;
    OUT.ind_assignments_qCE{t} = ind_assignments_qCE;

    % Check existence of (restricted/quasi) competitive equilibrium.
    OUT.CE{t}  = ~cellfun(@isempty, ind_assignments_CE)';
    OUT.qCE{t} = ~cellfun(@isempty, ind_assignments_qCE)';

    % Check whether economcies are undersupplied.
    OUT.undersupplied{t} = OUT.qCE{t} & ~OUT.CE{t};

    if all(~OUT.undersupplied{t})
        % Go to step 3
        break;
    end

    
    
    
    % Pick one undersupplied economy (suppliers in this economy will experience price increase). 
    economy_critical = break_tie( find(OUT.undersupplied{t}), rule );
    
    % Step 2.1
    % - For every i and S, if S not in L_i(p) then increase price p_i(S) by 1, else do not change the price p_i(S). 
    % - Go to Step (S1).
    prices_new = OUT.prices{t};
    % For each suppliers in critical economy (not including the "in house" supplier).
    for i = setdiff(1:n, economy_critical-1)
        neg_supply_sets = setdiff(1:m, OUT.supply_sets{t}{i}); % bundles not demanded
        prices_new{i}(neg_supply_sets) = prices_new{i}(neg_supply_sets) + 1;
    end
    OUT.prices{t+1} = prices_new;
    t = t + 1;

    % Go to step 1
    
    
end
if t == t_limit
    warning('t reached the t_limit'); 
else
    OUT.error_time = false;
end



% Step 3
% - The auction ends. 
% - If p is the final price vector in the auction, then a final allocation X in D(B, p) is chosen such that the number of suppliers who get a bundle from their supply set is maximized. 
% - The final payment of supplier i is p_i(X_i) + [ \pi^m(B_{-i}, p) - \pi^m(B, p) ]

ind_final_allocaion = break_tie(OUT.ind_assignments_CE{t}{1}, rule);
OUT.allocation = all_assignments{1}(ind_final_allocaion, :);
OUT.payment = [0; cellfun(@(p,i) p(OUT.allocation(i+1)), OUT.prices{t}, num2cell(1:n)') + cell2mat(OUT.procurement_cost{t}(2:end)) - repmat(OUT.procurement_cost{t}{1}, n, 1)]';


end

