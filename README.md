Implementation of the proposed auction in _"When hours touch hours: an efficient electricity procurement auction"_ in MATLAB. 

Included is also an implementation of the Vickrey-Dutch procurement auction from Mishra, D. & Parkes, D. C. (2007). Ascending price Vickrey auctions for general valuations. _Journal of Economic Theory_, 132(1), 335–366.