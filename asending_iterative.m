function OUT = asending_iterative(all_assignments, c, c0, t_limit, rule, varargin)
%ASENDING_ITERATIVE auction for procurement [based on @Baranov_2018]
%   Assuming truthful bidding.


% Parse optional variables
func_parser = inputParser;
addOptional(func_parser, 'verbose', false);
addOptional(func_parser, 'epsilon', 1);
parse(func_parser,varargin{:})
verbose = func_parser.Results.verbose;
epsilon = func_parser.Results.epsilon;

% Initialize the function
n = length(c);
dimensions = size(c0);
M = dimensions - ones(size(dimensions));
m = numel(c0);
if ~exist('verbose','var'); verbose = false; end
% Reformat each assignment to matrix form
all_assignments_matrix{1} = assign2matrix(all_assignments{1}, n+1, m); % Main economy
all_assignments_matrix(2:(n+1)) = repmat({assign2matrix(all_assignments{2}, n, m)}, 1, n); % Marginal economies
% Output metadata
OUT.class = 'asending_iterative';
OUT.c = c;
OUT.c0 = c0;
OUT.error_time = true;
OUT.epsilon = epsilon;

% Step 1
% - Initialize prices at zero.
t = 1;
OUT.prices{t} = zeros(size(M));


while t < t_limit
    if verbose; fprintf('Round %d ... \n', t); end

    prices_t = reshape_price(OUT.prices{t}, M);

    % Step 2
    % - Each supplier i reports supply-discount pair
    % - Each supplier i reports relative marginal cost for newly revealed bundles.
    for i = 1:n
        % Return the supply sets of supplier i
        [supply_sets, ~] = supply_correspondence(c{i}, prices_t); % [Assuming truthful bidding]
        OUT.supply_sets{t}{i} = supply_sets;
        supply = break_tie(supply_sets, rule);
        OUT.supply_set{t}(i) = supply;
        
        % TO-DO: implement activity rule. Currently, only partial warning given when activity rule violated..
        if t > 1
            history_supply_set = cell2mat(OUT.supply_set(1:end-1)');
            history_supply_set_i = history_supply_set(:,i);
            % Less or greater than vector x <= y, with at least one element
            % k in vector that is strictly less than x^k < y^k.
            history_supply_set_i_diff = all(ind2subm(dimensions, supply)-1 <= ind2subm(dimensions, history_supply_set_i')-1,2);
            history_supply_set_i_diff_strict = any(ind2subm(dimensions, supply)-1 < ind2subm(dimensions, history_supply_set_i')-1,2);
            if any( history_supply_set_i_diff & history_supply_set_i_diff_strict )
                warning("Activity rule 1 violation: Supplier %d's supply at time %d is subset of previous supply bundle.",i,t)
            end
        end

        OUT.marginal_costs{t}{i} = nan(dimensions);
        if t == 1
            OUT.revealed_bundles{t}{i} = bundle_subsets(supply, dimensions);
            newly_revealed = OUT.revealed_bundles{t}{i};
            OUT.bonus{t}(i) = nan;
        else
            OUT.revealed_bundles{t}{i} = unique([OUT.revealed_bundles{t-1}{i} bundle_subsets(supply, dimensions)]);

            newly_revealed = setdiff(OUT.revealed_bundles{t}{i}, OUT.revealed_bundles{t-1}{i});

            supply_prior = OUT.supply_set{t-1}(i);
            OUT.bonus{t}(i) = (prices_t(supply) - c{i}(supply)) - (prices_t(supply_prior) - c{i}(supply_prior)); % [Assuming truthful bidding]
        end
        OUT.marginal_costs{t}{i}(newly_revealed) = c{i}(newly_revealed) - c{i}(supply); % [Assuming truthful bidding]
    end

    
    % Step 3
    % - For each supplier i construct revealed bundles and cost function
    % approximation.
    % - Calculate undersupply 
    for i = 1:n

        % Calculate implied change in cost
        supply_sub = cell(size(M));
        supply_prior_sub = cell(size(M));
        [supply_sub{:}] = ind2sub(dimensions, OUT.supply_set{t}(i));
        if t == 1
           dx = cell2mat(supply_sub);
        else
           [supply_prior_sub{:}] = ind2sub(dimensions, OUT.supply_set{t-1}(i));
           dx = cellfun(@minus, supply_sub, supply_prior_sub);
        end
        OUT.dc_implied{t}(i) = OUT.prices{t} * dx' - OUT.bonus{t}(i);

        
        % Calculate revealed relative marginal cost
        OUT.marginal_costs_revealed{t}{i} = nan(dimensions);
        for s = OUT.revealed_bundles{t}{i}
            [is_prior_supply, t_s] = ismember(s, cellfun(@(x) x(i), OUT.supply_set));
            if is_prior_supply
                OUT.marginal_costs_revealed{t}{i}(s) = - sum( cellfun(@(x) x(i), OUT.dc_implied((t_s+1):t)) );
            else
                t_s_revealed = find(~isnan(cellfun(@(x) x{i}(s), OUT.marginal_costs)));
                OUT.marginal_costs_revealed{t}{i}(s) = - sum( cellfun(@(x) x(i), OUT.dc_implied((t_s_revealed+1):t)) ) + OUT.marginal_costs{t_s_revealed}{i}(s);
            end
        end

        
        % Calculate GARP function
        supply = OUT.supply_set{t}(i);
        % All constants (i.e. one for each time period, satisfying GARP condition in that period. These constants are exactly "small enough").
        OUT.c_garp_all{t}{i} = cellfun(@(x) ...
                sum( cell2mat(OUT.prices') .* ( ind2subm(dimensions, cellfun(@(x) x(i), OUT.supply_set)) - ind2subm(dimensions, x) ), 2) ...
                + flip(cumsum( flip([cellfun(@(x) x(i), OUT.dc_implied(2:t)) 0]) ))' ...
                + sum( OUT.prices{t} .* ( ind2subm(dimensions, x) - ind2subm(dimensions, supply) ), 2), ...
            num2cell(1:m), 'UniformOutput', false);
        % Select the minimum constant (i.e. satisfy the GARP condition in every time period).
        OUT.c_garp{t}{i} = reshape( cellfun(@min, OUT.c_garp_all{t}{i}), dimensions);
        
        % Calculate cost function approximation
        OUT.c_approx{t}{i} = nan(dimensions);
        revealed_bundles = OUT.revealed_bundles{t}{i};
        OUT.c_approx{t}{i}(revealed_bundles) = prices_t(supply) + OUT.marginal_costs_revealed{t}{i}(revealed_bundles);        
        non_revealed_bundles = setdiff(1:m, revealed_bundles);
        OUT.c_approx{t}{i}(non_revealed_bundles) = sum(OUT.prices{t} .* (ind2subm(dimensions, non_revealed_bundles)-1), 2)' - OUT.c_garp{t}{i}(non_revealed_bundles); 

        % Calculate revealed supply correspondence
        payoff_revealed = prices_t(revealed_bundles) - OUT.c_approx{t}{i}(revealed_bundles);
        max_payoff_revealed = max(payoff_revealed);
        OUT.supply_correspondence_revealed{t}{i} = revealed_bundles(find(payoff_revealed == max_payoff_revealed));

    end
    
    % Add "in house" supplier
    OUT.c_approx{t} = {c0, OUT.c_approx{t}{:}}; % Approximate cost equal c0
    
    % Tentative allocation
    % Loop through each economy (1 main and n marginal)
    tentative_assignments   = cell(n+1, 1);
    procurement_cost        = cell(n+1, 1);
    for e = 1:(n+1)

        % Prepare/reformat cost function matrix (and remove row of supplier e in marginal economies)
        c_approx_matrix_e = reshape2mat(OUT.c_approx{t}');
        if e ~= 1; c_approx_matrix_e(e,:) = []; end

        % Return the demand set / tentative assignment
        [tentative_assignments{e}, procurement_cost{e}] = demand_correspondence(all_assignments_matrix{e}, c_approx_matrix_e);
        
        
        % Undersupply / Market clearing
        
        % Prepare (remove coloumn of supplier e in marginal economies)
        supply_correspondence_revealed_e = OUT.supply_correspondence_revealed{t};
        revealed_bundles_e = OUT.revealed_bundles{t};
        if e ~= 1 
            supply_correspondence_revealed_e(e-1) = []; 
            revealed_bundles_e(e-1) = [];
        end
        
        % Total undersupply: Set of all vectors
        [total_undersupply_set, ta_e] = undersupply(dimensions, all_assignments{e}(:,2:end), tentative_assignments{e}, supply_correspondence_revealed_e, revealed_bundles_e);
        OUT.total_undersupply_set{t}{e} = total_undersupply_set;
        
        % Pick the total undersupply vector that minimise current procurement cost of undersupply
        procurement_undersupply = sum(OUT.prices{t} .* total_undersupply_set, 2);
        min_idx = find(procurement_undersupply == min(procurement_undersupply));
        pick = break_tie(min_idx, rule);
        OUT.total_undersupply{t}{e} = total_undersupply_set( pick, : );
        OUT.assignment{t}{e} = all_assignments{e}(ta_e(pick), :);
       
        OUT.marketclear{t}(e) = all( OUT.total_undersupply{t}{e} == 0 );
        
    end
    OUT.tentative_assignments{t} = tentative_assignments;
    OUT.procurement_cost{t} = cell2mat(procurement_cost)';

    % Step 4
    % - if all markets do not clear adjust price and go to step 2
    % - otherwise terminate auction and determine supply and prayments.
    
    if all(OUT.marketclear{t})
        % Terminate auction
        break;
    end
    
    % Remove "in house" supplier in loop
    OUT.c_approx{t}(1) = [];
    
    % Price increase
    % Pick non-cleared economy e (only pick main economy once all marginal economies have cleared)
    if all(OUT.marketclear{t}(2:end))
        ee = 1; 
    else
        ee = break_tie(1 + find(OUT.marketclear{t}(2:end) == 0), rule);
    end
  
    %epsilon = 0.05;
    OUT.prices{t+1} = OUT.prices{t} + epsilon * (OUT.total_undersupply{t}{ee} > 0);
    t = t+1;

end
if t == t_limit
    warning('t reached the t_limit'); 
    OUT.prices(t_limit:end) = []; 
    t = t -1; 
else
    OUT.error_time = false;
end


% Allocation and payment
OUT.allocation = OUT.assignment{t}{1};
OUT.payment = cellfun(@(i) ...
                sum( ...
                    cellfun( ...
                        @(c,x,y) c(x) - c(y), ...
                        OUT.c_approx{t}( setdiff(1:(n+1),i) ), ...
                        num2cell(OUT.assignment{t}{i}( (1:n) + (i==1) )), ... 
                        num2cell(OUT.assignment{t}{1}( setdiff(1:(n+1),i) )) ...
                    ) ...
                ), ...
                num2cell(1:(n+1)) ...
              );
OUT.payment(1) = 0; % No payment to "in house" supplier.

if length(OUT.c_approx{t}) > n
   % Remove "in house" supplier at termination
    OUT.c_approx{t}(1) = [];
end
          
end

