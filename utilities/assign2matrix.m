function all_assignments_matrix = assign2matrix(all_assignments, n, m)
%ASSIGN2MATRIX Reformats assignments to matrix form

all_assignments_matrix = cell(size(all_assignments,1), 1);

for k = 1:size(all_assignments,1)
    assignment = all_assignments(k,:);
    
    all_assignments_matrix{k} = false(n, m);
    for i = 1:(n)
        all_assignments_matrix{k}(i, assignment(i)) = true;
    end
end

end

