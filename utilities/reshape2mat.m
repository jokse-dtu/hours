function y = reshape2mat(x)
%RESHAPE2MAT Reshapes each element in cell into vector and converts to matrix
% x is a cell where each element is identically sized.
    y = cell2mat(cellfun(@(x_i) x_i(:)', x, 'UniformOutput', false));
end

