function [supply_sets, profit] = supply_correspondence(c_i, p_i)
%SUPPLY_CORRESPONDENCE Returns the supply set of supplier i at prices p_i

% Indirect value function
values = p_i - c_i;

% Maximum profit
profit = max(values, [], 'all');

% Find corresponding supply sets
supply_sets = find(values == profit)'; 

end

