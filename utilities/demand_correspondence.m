function [ind_demand_sets, procurement_cost] = demand_correspondence(all_assignments_matrix, prices_matrix)
%DEMAND_CORRESPONDENCE Returns the demand set of buyer at prices p

% Brute force approach

% Calculate total procurement cost for every feasible assignment, given
% price and supply
procurement_cost_list = nan(length(all_assignments_matrix), 1);
for k = 1:length(all_assignments_matrix)
    procurement_cost_list(k) = sum(prices_matrix(all_assignments_matrix{k}));
end

% Minimum total procurement cost
procurement_cost = min(procurement_cost_list);

% Array with indicies of assignments (i.e. demand sets) that minimise
% procurement cost
ind_demand_sets = find(procurement_cost_list == procurement_cost);

end

