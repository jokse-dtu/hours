function m = rm(m,i)
%RM Removes row i from matrix m
    m(i,:) = [];
end

