function all_subsets = bundle_subsets(bundle, dimensions)
%BUNDLE_SUBSETS Return indcies of all bundles that are subsets of bundle.

% Convert bundle index intro subscripts
bundle_sub = cell(size(dimensions));
[bundle_sub{:}] = ind2sub(dimensions, bundle);

% Consider all lower subscripts 
subsets_sub = cellfun(@(x) 1:x, bundle_sub, 'UniformOutput', false);

% Consider all combinations of subscripts
all_subsets_sub = num2cell(combvec(subsets_sub{:}), 2);

% Convert subscripts of subsets into bundles indicies of subsets
all_subsets = sub2ind(dimensions, all_subsets_sub{:});

end

