function part = reshape_partition(dimensions, M_count, p)
%RESHAPE_PARTITION Summary of this function goes here
%   Detailed explanation goes here

part = zeros(dimensions); % template for partition
for k = 1:length(p)
    part_sub = ones(size(M_count)); 
    part_sub(p{k}) = 2; % subscripts
    part(array2ind(dimensions, part_sub)) = 1; % change partition
end

end

