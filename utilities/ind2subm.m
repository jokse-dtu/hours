function y = ind2subm(dimensions, x)
%IND2SUBM Same as ind2sub, but returns matrix.

y_cell = cell(1, length(dimensions));
[y_cell{:}] = ind2sub(dimensions, x);
y = cell2mat(y_cell')';

end

