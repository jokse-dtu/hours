function [total_undersupply_set, ta] = undersupply(dimensions, all_assignments_e, tentative_assignments_e, supply_correspondence_revealed_e, revealed_bundles_e)
%UNDERSUPPLY For economy M, calculates the total undersupply of every good.

    nn = length(supply_correspondence_revealed_e); % number of suppliers in economy M

    % All combinations of tentative assignments and individual revealed supply correspondences
    all_comb = combvec(tentative_assignments_e', supply_correspondence_revealed_e{:})';
    ta = all_comb(:,1);     % Tentive allocations
    sc = all_comb(:,2:end); % Vector of revealed supply correspondences (coloumn for each supplier i in economy)

    % Convert indcies into subscripts, and use subscripts to calculate change in number of items for each good.
    % Note order: % each cell {} is dimension; each coloumn is bidder; and each row is a different combination of tentative assignment and supply correspondece.
    sc_sub = cell(length(dimensions), 1);
    ta_sub = cell(length(dimensions), 1);
    [sc_sub{:}] = ind2sub(dimensions, sc);
    [ta_sub{:}] = ind2sub(dimensions, all_assignments_e(ta,:));
    items_diff = cellfun(@minus, ta_sub, sc_sub, 'UniformOutput', false); 
    
    % Check if supplier i's tentive assignment is not in its set of revealed bundles.
    assignment_i = mat2cell(all_assignments_e(ta,:), length(ta), ones(1,nn));
    isnot_revealed_bundle = cell2mat( cellfun(@(x,y) ~ismember(x,y), assignment_i, revealed_bundles_e, 'UniformOutput', false) );

    % Individual undersupply
    % Change in number of items, if this change is positive, and if allocation is not in revealed bundles. Otherwise zero.
    zeta = cellfun(@(x) x .* double(x>0 .* isnot_revealed_bundle), items_diff, 'UniformOutput', false);

    % Total undersupply: Set of all vectors
    total_undersupply_set = cell2mat( cellfun(@(x) sum(x,2), zeta, 'UniformOutput', false)' );


end

