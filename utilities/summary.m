function o = summary(OUT, labels)
%SUMMARY prints a summary of the auction process.

switch OUT.class
    
    case 'vickrey_dutch'
        
        t = length(OUT.prices);
        n = length(OUT.c);
        m = numel(OUT.c0);
        SUMM.t = (1:t)';

        % Prices 
        prices_matrix = {};
        for i=1:n
            prices_matrix{i} = cell2mat(cellfun(@(x) x{i}(:), OUT.prices, 'UniformOutput', false))'; % bidder i
            SUMM.(sprintf('prices_%.f', i)) = cellstr(num2str(prices_matrix{i}, '%d '));
        end

        SUMM.procurement_cost = cellstr(num2str( cell2mat(cellfun(@(x) cell2mat(x), OUT.procurement_cost, 'UniformOutput', false))' , '%d '));

        % Display table
        o = struct2table(SUMM)
        if ~exist('labels','var'); labels = string(1:m); end
        fprintf("Allocation: \t %s \n", join(labels(OUT.allocation)) );
        fprintf("Payment: \t %s \n", join(string(OUT.payment)) );
        fprintf("Cost: \t\t %s %s \n", string(OUT.c0(OUT.allocation(1))), join(string(cell2mat(cellfun(@(x,i) x(OUT.allocation(i+1)), OUT.c, num2cell(1:n), 'UniformOutput', false)))) ); 
        fprintf("Payoff: \t %s %s \n", string(OUT.payment(1) - OUT.c0(OUT.allocation(1))),  join(string(cell2mat(cellfun(@(x,i) OUT.payment(i+1) - x(OUT.allocation(i+1)), OUT.c, num2cell(1:n), 'UniformOutput', false)))) );

        
    case 'asending_iterative'
        
        t = length(OUT.prices);
        n = length(OUT.c);
        m = numel(OUT.c{1});
        SUMM.t = (1:t)';
        
        SUMM.prices = cell2mat(OUT.prices');
        
        if ~exist('labels','var'); labels = string(1:m); end
        SUMM.supply = labels(cell2mat(OUT.supply_set'));
        SUMM.revealed_bundles = cellstr(cellfun(@(y) join(cellfun(@(x) replace("{"+join(labels(x))+"}", "{"+join(labels(:))+"}", char(937))+" ", y), " "), OUT.revealed_bundles, 'UniformOutput', false))';
        for i = 1:n
            SUMM.(sprintf('c_approx_%.f', i)) = cell2mat(cellfun(@(x) x{i}(:)', OUT.c_approx, 'UniformOutput', false)');
        end
        SUMM.total_undersupply = cellfun(@(x) join(cellfun(@(y) "("+join(string(y))+")", x), "  "), OUT.total_undersupply)';
        
        % Display table
        o = struct2table(SUMM)
        fprintf("Allocation: \t %s \n", join(labels(OUT.allocation)) );
        fprintf("Payment: \t %s \n", join(string(OUT.payment)) );
        fprintf("Cost: \t\t %s %s \n", string(OUT.c0(OUT.allocation(1))), join(string(cell2mat(cellfun(@(x,i) x(OUT.allocation(i+1)), OUT.c, num2cell(1:n), 'UniformOutput', false)))) ); 
        fprintf("Payoff: \t %s %s \n", string(OUT.payment(1) - OUT.c0(OUT.allocation(1))),  join(string(cell2mat(cellfun(@(x,i) OUT.payment(i+1) - x(OUT.allocation(i+1)), OUT.c, num2cell(1:n), 'UniformOutput', false)))) );

        
end

end

