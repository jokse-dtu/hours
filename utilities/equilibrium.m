function ind_assignments_equilibrium = equilibrium(all_assignments_e, demand_set_e, supply_sets_e)
%EQUILIBRIUM Summary of this function goes here
%   Detailed explanation goes here

ind_compatible = false(size(demand_set_e));

for k = 1:length(demand_set_e)
    assignment = num2cell( all_assignments_e(demand_set_e(k), 2:end) ); % remove "in house" supplier
    ind_compatible(k) = all(cellfun(@(x,y) any(ismember(x,y)), supply_sets_e, assignment));
end    

ind_assignments_equilibrium = demand_set_e(ind_compatible);

end

