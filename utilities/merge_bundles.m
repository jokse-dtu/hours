function y = merge_bundles(a, b, dimensions)
%MERGE_BUNDLES Return the union of a pair of bundles of goods.

a_vector_cell = cell(1, length(dimensions));
b_vector_cell = cell(1, length(dimensions));
[a_vector_cell{:}] = ind2sub(dimensions,a);
[b_vector_cell{:}] = ind2sub(dimensions,b);

y_vector = (cell2mat(a_vector_cell)-1) + (cell2mat(b_vector_cell)-1) + 1;
y_vector_cell = num2cell(y_vector);
if all(y_vector <= dimensions)
    y = sub2ind(dimensions, y_vector_cell{:});
else
    warning("Merging bundles where intersection is non-empty.")
    y = max([a b]);
end


end

