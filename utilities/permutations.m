function pp = permutations(n, k)
% PERMUTATIONS Return all possible permutations
    nk = nchoosek(1:n, k);
    pp = zeros(0, k);
    for i=1:size(nk, 1),
        pi = perms(nk(i, :));
        pp = unique([pp; pi], 'rows');
    end
end