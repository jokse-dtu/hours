function all_assignments = assignments(all_partitions, n)
%ASSIGNMENTS Returns all feasible assignments.

    % Return feasible assignments for every partition and merge.
    all_assignments = cell2mat( cellfun(@(p) part2assign(p, n), all_partitions, 'UniformOutput', false) );
    
    % Remove duplicates.
    all_assignments = unique(all_assignments, 'rows');
    % Remove assignment with only null bundles
    ind_null = find(ismember(all_assignments, ones(1,n), 'rows'));
    all_assignments(ind_null, :) = [];
    
end

function assignments_p = part2assign(p, n)
% PART2ASSIGN Takes partition p and number of suplliers n as input. Returns 
% a n-wide matrix, where each row is a unique assignments of bundles in the 
% partition. 

    if length(p) > n
        % Ignoring partition, since number of items in bundle exceeds the 
        % number of suppliers.
        assignments_p = [];
    else
        % Uses that the partition does not include null bundle. We ensure
        % a sufficent variations (i.e. number of bundles) using fixed length of 
        % n. These "additional bundles" are all subsequently replaced with 
        % the null bundle.
        if length(p) < n
            p(n) = 0; % Extend partition p to length n.
            p(p==0) = 1; % Change from matlab default to index of null bundle.
        end

        % Return all permutations (i.e. all assignments of items among suppliers).
        assignments_p = perms(p);
    end 
end