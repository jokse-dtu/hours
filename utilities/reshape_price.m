function prices = reshape_price(p, M)
%RESHAPE_PRICE Takes linear price vector p and outputs matrix with price
% per bundle.


% Create cell for each type of good with array of number items; from 0 to max number of items
items = cellfun(@(x) (0:x), num2cell(M), 'UniformOutput', false);

% For each good create matricies of correct dimensions and where each
% element currespond to the number of items of the good.
items_mesh = cell(1, length(M));
[ items_mesh{:} ] = ndgrid( items{:} );

% Multiple the price of each good with the number of items of each good.
prices_mesh = cellfun(@(p,x) p*x, num2cell(p), items_mesh, 'UniformOutput', false);

% Sum prices over goods creating bundle prices.
dim = ndims(prices_mesh{1}) + 1;
prices = sum( cat(dim, prices_mesh{:}), dim);

end

