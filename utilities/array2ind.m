function ind = array2ind(dim, array)
%ARRAY2IND Convert array of coordinates into linear indices.
% See also, ind2sub, sub2ind
list = num2cell(array);
ind = sub2ind(dim, list{:});
end

