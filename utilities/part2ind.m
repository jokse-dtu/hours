function ind = part2ind(p, M)
%PART2IND The function takes a partition as input and outputs an array with 
% the indecies of every bundle in the partition.
%   The function uses the fact that all the elements, which make up a 
%   bundle, are single units of an item: 
%   First, calculate the index of each element. 
%   Second, merge the elements iteratively to get index of entire bundle.

dimensions = ones(size(M)) + M; % Dimensions of suppliers' cost

ind = zeros(1, length(p)); % prepare array of bundle indicies

% For each bundle in the partition
for k = 1:length(p)
    bundle = p{k};
    
    % Returns an array with the item-indecies for every element in the bundle.
    [~, ind_elements_item] = max(bundle <= repmat(cumsum(M), length(bundle), 1)', [], 1);
    
    % Returns an array with the indecies for every element in the bundle
    % (using that the elements, which make up a bundle, are single unit items). 
    ind_elements = arrayfun(@(item) item2ind(item, M) , ind_elements_item);
    
    % Apply merge_bundles() function iteratively to array of indcies
    while length(ind_elements) > 1
        %ind_elements % Debug tool: preview
        ind_merged = merge_bundles(ind_elements(1), ind_elements(2), dimensions);
        ind_elements(1) = []; % Removes element from beginning of array
        ind_elements(1) = ind_merged; % Replaces first element in array with merged bundle index
    end
    ind(k) = ind_elements; % Last remaining index
    
end

ind = sort(ind); % Sort array of indecies in preperation for uniquecell()

end


function ind_element = item2ind(item, M)
    % Returns the index of the first unit of an item.
    
    dimensions = ones(size(M)) + M; % Dimensions of suppliers' cost
    
    % Create subscript index array of element
    sub = ones(1, length(M)); % Subscript of every item is 1.
    sub(item) = 2; % Subscript of this specific item is 2.
    
    % Convert subscript to index, akin to sub2ind()
    ind_element = array2ind(dimensions, sub);
    
end

