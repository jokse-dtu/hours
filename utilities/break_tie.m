function y = break_tie(x, tie_breaking_rule)
%BREAK_TIE Picks one element in the set of alternatives x using the 
% specified tie breaking rule.

    switch tie_breaking_rule
        case 'first'
            pick = 1;
        case 'last'
            pick = length(x);
        case 'random'
            pick = randsample(length(x),1);
    end
    
    if iscell(x)
        y = x{pick};
    else
        y = x(pick);
    end
    
end