% See model discription in README
addpath(genpath('utilities'))

% We follow Matlab's subscript layout and notation.
% https://mathworks.com/help/matlab/ref/ind2sub.html (examples 1-2).
% First item along vertical axis, second item along horizontal axis, 
% third item along "depth" axis, etc.
% Examples:
% labels = ["0", "B"; "A", "AB"];
% labels = ["0", "C"; "A", "AC"; "AB", "ABC"]; % A = B, AC = BC
% labels = cat(3, ["0", "B"; "A", "AB"], ["C", "BC"; "AC", "ABC"]);


% -------------------------------------------------------------------------

% Example 1
M = [1 1]; % Number of units of each type of item.
c = {[0 3; 2 8] [0 5; 5 8]}; % Suppliers' cost
c0 = 10 * [0 6; 6 9]; % "In-house" cost
%labels = ["0", "B"; "A", "AB"];
labels = ["(0,0)", "(0,1)"; "(1,0)", "(1,1)"];


% -------------------------------------------------------------------------
% Setup
n = length(c); % Number of suppliers
dimensions = ones(size(M)) + M; % Dimensions of suppliers' cost
m = prod(dimensions);

% Set of possible partitions (Using https://mathworks.com/matlabcentral/fileexchange/24185-partitions)
all_partitions_list = partitions(sum(M));

% Reformat using index of each bundle in every partition
all_partitions_raw = cellfun(@(p) part2ind(p, M), all_partitions_list, 'UniformOutput', false);
all_partitions = uniquecell(all_partitions_raw); % Using https://mathworks.com/matlabcentral/fileexchange/31718-unique-elements-in-cell-array

% Set of feasible assignments for the main economy and any of the marginal economies.
all_assignments{1} = assignments(all_partitions, n+1); % Main economy
all_assignments(2:(n+1)) = repmat({assignments(all_partitions, n)}, 1, n); % Marginal economies


% -------------------------------------------------------------------------
% Efficient allocation and minimum procurement costs under perfect information.

% Calculate efficient allocation using demand_correspondence where
% individual prices are replaced with individual costs. Also returns the
% minimum procurement costs.
[eff_allocations, eff_procurement_cost] = ... 
    demand_correspondence( ...
        assign2matrix(all_assignments{1}, n+1, m), ...
        [ ...
            c0(:)'; ...
            cell2mat(cellfun(@(x) x(:), c, 'UniformOutput', false))' ...
        ] ...
    );

labels( all_assignments{1}(eff_allocations,:) ) % Efficient allocations.
eff_procurement_cost % Minimum total procurement costs


% -------------------------------------------------------------------------
% Vickrey-Dutch auction (VDA) for procurement [@Mishra_Veeramani_2007, definition 4]

OUT1 = vickrey_dutch(all_assignments, c, c0, 100, 'first');
summary(OUT1, labels);


% -------------------------------------------------------------------------
% Asending iterative auction for procurement based on @Baranov_2018
OUT2 = asending_iterative(all_assignments, c, c0, 100, 'random', 'epsilon', 3);
summary(OUT2, labels);


